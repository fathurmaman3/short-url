@extends('layouts.template')
@section('content')

@if (session('success'))
<div class="alert alert-success">{{ session('success') }}</div>
@endif


<div class="card">
    <div class="card-header">
        <form method="post" action=" {{ route('url.store') }} ">
            @csrf
            {{--  @method('PUT')  --}}
            <div class="form-row">

                <input type="text" name="user_id" class="form-control" id="user_id" value="{{ $user_id }}" hidden>
                <input type="text" name="status" class="form-control" id="status" value="1" hidden>

                <div class="form-group col">
                    <label for="url">Link Asli</label>
                    <input type="text" name="link" class="form-control" id="url" placeholder="Masukkan Link">
                @error('link')
                    <p class="m-0 p-0 text text-danger">{{ $message }}</p>
                @enderror

                </div>


                <div class="form-group col">
                    <label for="custom">Kustomisasi Link</label>
                    <input type="text" name="custom"
                    class="form-control @error('custom')is-invalid @enderror"
                    id="custom"
                    placeholder="Masukkan Nama Link">
                @error('custom')
                    <p class="m-0 p-0 text text-danger">{{ $message }}</p>
                @enderror
                </div>

            </div>

            <div class="input-group-addon">
                <button class="btn btn-success">Simpan Link</button>
            </div>
        </form>

    </div>
    <div class="card-body">
        @csrf @method('delete')
        <table id="url" class="display table" style="table-layout: fixed; width: 100%;" >
        <thead>
            <tr>
                <th scope="col" class="text-center" style="width: 5%">No</th>
                <th scope="col" class="text-center" style="width: 30%">Link</th>
                <th scope="col" class="text-center" style="width: 40%">Link Tujuan</th>
                <th scope="col" class="text-center" style="width: 15%">Status</th>
                <th scope="col" class="text-center" style="width: 10%">Aksi</th>
            </tr>
        </thead>
        <tbody>
                @foreach ($latestLink->where('user_id', $user_id) as $row)
                <tr>
                    <td class="text-center">{{ $loop->index+1 }}</td>
                    <td>
                        @if ($row->status == true)
                            <a href="{{ route('url.latestUrl', $row->code) }}" target="_blank">{{ route('url.latestUrl', $row->code) }}</a>
                        @else
                            <a style="text-decoration:none; color:dimgray" href= @disabled(true)"{{ route('url.latestUrl', $row->code) }}" target="_blank">{{ route('url.latestUrl', $row->code) }}</a>
                        @endif
                    </td>
                    <td>{{ $row->link }}</td>
                    <td class="text-center">
                        @if ( $row->status  == 1)
                            <span class="badge badge-success">Aktif</span>
                        @else
                            <span class="badge badge-secondary">Non Aktif</span>
                        @endif

                    </td>
                    <td>
                        <div class="d-flex justify-content-around">
                            <div class="p-2">
                                <a href="{{ route('url.edit', $row->id) }}"><i class="fas fa-edit"></i></a>
                            </div>
                            <div class="p-2">
                                <a href="#"><i class="fas fa-trash-alt" style="color:red" data-toggle="modal" data-target="#deleteModal-{{ $row->id }}"></i></a>
                                <div class="modal fade" id="deleteModal-{{ $row->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                        aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Apakah anda ingin menghapus data ini?</h5>
                                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <div class="modal-footer">

                                                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                                                    <form action="{{ route('url.destroy', $row->id) }}" method="POST">
                                                        @csrf
                                                        @method('delete')
                                                        <button class="btn btn-primary" href="{{ route('url.destroy', $row->id) }}">Hapus</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
    </table>
    </div>

</div>
@endsection

@section('scripts')
    <script>
        $(document).ready( function () {
            $('.display').DataTable({
                "searching": false,
                "pageLength": 1,
                "lengthChange": false
            })
        } );

    </script>
@endsection
