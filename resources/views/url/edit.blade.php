@extends('layouts.template')
@section('content')

@if (session('success'))
<div class="alert alert-success">{{ session('success') }}</div>
@endif


<div class="card">
    <div class="card-header">
        <form method="post" action=" {{ route('url.update', $linkid->id) }} ">
            @csrf
            @method('put')
            <div class="form-row">
                <div class="form-group col">
                    <label for="url">Destination</label>
                    <input type="text" name="link" class="form-control" id="url" placeholder="Enter URL" value="{{ $linkid->link }}">
                @error('link')
                    <p class="m-0 p-0 text text-danger">{{ $message }}</p>
                @enderror

                </div>

                <div class="form-group col">
                    <label for="custom">Custom back-half </label>
                    <input type="text" name="custom" class="form-control" id="custom" placeholder="Enter Custom" value="{{ $linkid->code }}">
                @error('custom')
                    <p class="m-0 p-0 text text-danger">{{ $message }}</p>
                @enderror

                </div>

                <div class="form-group">
                    <label for="status">Aktifasi</label>
                    <select class="form-control" id="status" name="status">
                    @if ($linkid->status == true)
                        <option value=1>Aktif</option>
                        <option value=0>Non Aktif</option>
                    @else
                        <option value=0>Non Aktif</option>
                        <option value=1>Aktif</option>
                    @endif
                    </select>
                </div>

            </div>
            <div class="input-group-addon">
                    <button type="submit" class="btn btn-success">Simpan</button>
            </div>
        </form>

    </div>
</div>
@endsection
