<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>SHORT URL</title>

    <!-- Custom fonts for this template-->
    @include('partials.style')
  </head>

    <body class="bg-gradient-primary">
        <div class="container">
        <!-- Outer Row -->
        <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-10 col-md-7">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-4 d-none d-lg-block mt-4 mb-4 mr-5">
                        <img src="{{ url('template/img/pemprov.png') }}" style="max-width:360px;" alt="pemprov-riau">
                    </div>
                    <div class="col-lg-6">
                    <div class="p-5">
                        <form class="user" action="{{ route('login') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <input
                                type="email"
                                class="form-control form-control-user @error('email') is-invalid @enderror" name="email" required autocomplete="email" autofocus/>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input
                                type="password"
                                class="form-control form-control-user form-control @error('password') is-invalid @enderror"name="password" required autocomplete="current-password"

                                />
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <button type="submit"
                                class="btn btn-primary btn-user btn-block"
                            >
                                Login
                            </button>
                        </form>
                        <hr />
                    </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>

        @include('partials.script')
    </body>
</html>


