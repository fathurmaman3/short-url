<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
    use HasFactory;
    protected $table = "url";
    protected $primaryKey = "id";
    protected $fillable = ['code','link', 'user_id', 'status'];


    public function User(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function getLatestData()
    {
        return $this->orderBy('created_at', 'desc')->get();
    }

}
